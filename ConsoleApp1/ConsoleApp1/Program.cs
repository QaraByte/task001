﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите имя:");
            var s = Console.ReadLine();
            ElementList<string> list = new ElementList<string>();
            list.Add(s);
            list.Add("Ivan");
            list.Add("John");
            list.Add("Bauyrzhan");
            list.Add("Wick");
            list.AddByIndex("Masha", 12);
            list.RemoveByIndex(3);
            Console.WriteLine("Количество элементов: " + list.Count());
            Console.WriteLine("Введите элемент списка, чтобы получить значение:");
            int el = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Имя под индексом " + el + ": " + list.GetElement(el));

            foreach (var item in list)
            {
                Console.Write(item + " ");
            }
            Console.ReadKey();
        }
    }

    //Класс элемента списка
    public class Element<T>
    {
        public Element(T data)
        {
            DATA = data;
        }
        public T DATA { get; set; }
        public Element<T> NEXT { get; set; }
        public Element<T> PREVIOUS { get; set; }
    }

    //Класс самого списка
    public class ElementList<T>:IEnumerable
    {
        Element<T> head;
        Element<T> tail;
        int count;

        public void Add(T data)
        {
            Element<T> el = new Element<T>(data);
            if (head == null)
                head = el;
            else
            {
                tail.NEXT = el;
                el.PREVIOUS = tail;
            }
            tail = el;
            count++;
        }

        public void AddByIndex(T data, int index)
        {
            if (index > count || index < 0)
                return;
            Element<T> temp = head;
            int _index = 0;
            while (temp.NEXT != null)
            {
                if (_index == index)
                {
                    Element<T> el = new Element<T>(data);
                    el.NEXT = temp;
                    el.PREVIOUS = temp.PREVIOUS;
                    el.PREVIOUS.NEXT = el;
                    el.NEXT.PREVIOUS = el;
                    head = ToFirst(el);
                    count++;
                    break;
                }
                else
                    temp = temp.NEXT;

                _index++;
            }
        }

        public Element<T> ToFirst(Element<T> t)
        {
            while (t.PREVIOUS != null)
            {
                t = t.PREVIOUS;
            }
            return t;
        }

        public void RemoveByIndex(int index)
        {
            int _index = 0;
            Element<T> temp = head;
            while (temp.NEXT != null)
            {
                if (_index == index)
                {
                    Element<T> el = temp.PREVIOUS;
                    el.NEXT = temp.NEXT;
                    el.PREVIOUS.NEXT = el;
                    el.NEXT.PREVIOUS = el;
                    head = ToFirst(el);
                    count--;
                    break;
                }
                else
                    temp = temp.NEXT;

                _index++;
            }
        }

        public int Count()
        {
            return count;
        }

        public string GetElement(int index)
        {
            Element<T> temp = head;
            for (int i = 0; i < count; i++)
            {
                if (i == index)
                {
                    return temp.DATA.ToString();
                }
                else
                    temp = temp.NEXT;
            }
            return "Элемент не найден.";
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        
        public IEnumerator GetEnumerator() // Этот метод позволяет использовать список в конструкции foreach.
        {
            if (this.count == 0)
                yield break;
            Element<T> el = head;
            while (el != null)
            {
                yield return el.DATA;
                el = el.NEXT;
            }
        }
    }
}
